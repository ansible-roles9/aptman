# [0.8.0](/Ansible_Roles/aptman/compare/0.8.0...main)
* Hide sensitive information like credentials, attending the issue #11

# [0.7.0](/Ansible_Roles/aptman/compare/0.7.0...main)
* Giving support to authenticate on repositories that require it

# [0.6.0](/Ansible_Roles/aptman/compare/0.6.0...main)
* CICD migraton from Jenkins to GitlabCI
* Update exceptions for **snap** and **flatpak** in order to work with *Gitlab*

# [0.5.0](/Ansible_Roles/aptman/compare/0.5.0...main)
* Se configuran las versiones del `CHANGELOG.md` para que sean revisables por la herramienta de comparación de **Gitea**

# [0.4.0](/Ansible_Roles/aptman/compare/0.4.0...main)
* Se actualiza el jenkinsfile segun la nueva versión de la *Shared Library*

# [0.3.0](/Ansible_Roles/aptman/compare/0.3.0...main)
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados

# [0.2.4](/Ansible_Roles/aptman/compare/0.2.4...main)
* Se ajusta el `.editorconfig`, `.gitignore` con las nuevas reglas bajo el estándar de plantilla
* Se ajusta el `Jenkinsfile` con nuevas reglas de validación

# [0.2.3](/Ansible_Roles/aptman/compare/0.2.3...main)
* Se habilita funcionalidad en `ansiblePipeline` para elegir la plataforma donde se quiere testar, se habilita el testeo en *amd64*

# [0.2.2](/Ansible_Roles/aptman/compare/0.2.2...main)
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `ansiblePipeline`
* Se ajusta el `README` para que muestre información relevante

# [0.2.1](/Ansible_Roles/aptman/compare/0.2.1...main)
* Debido a un error puntual en la desinstalación de un paquete `flatpak`, se mejora la documentación [ issue: #3 ]

# [0.2.0](/Ansible_Roles/aptman/compare/0.2.0...main)
* No se había declarado previamente la lista `main_packages`, es posible que esto provoque un fallo inesperado si se declara el rol sin pasar argumentos a esta variable
* Habilitamos soporte para configurar paquetes en estdo `hold` (Issue #1)
* Habilitamos soporte para gestionar paquetes `flatpak` (Issue #2)

# [0.1.7](/Ansible_Roles/aptman/compare/0.1.7...main)
* Actualizamos `Jenkinsfile` para adaptarnos a la nueva versión del vars `ansibleActions`[jenkins-shared-libraries]

# [0.1.6](/Ansible_Roles/aptman/compare/0.1.6...main)
* Se agrega control del rol para que opere solo en distribuciones basadas en *Debian*

# [0.1.5](/Ansible_Roles/aptman/compare/0.1.5...main)
* Quitamos la opción *force_apt_get*, estando antes en **true**, se ha comprobado que puede crear problemas al momento de instalar paquetes partiendo de una ***base de datos apt*** sin cache.
* Se renombra la variable *main_base* por *requiredpackages* a fin de seguir con el estándar de paquetes requeridos por cada rol.

# [0.1.4](/Ansible_Roles/aptman/compare/0.1.4...main)
* Dado que se elimina el testing sobre docker de las tareas *snapd*, se elimina el bloque *pre-task* que instalaba el paquete *snapd* para agilizar el testing.

# [0.1.3](/Ansible_Roles/aptman/compare/0.1.3...main)
* Se corrige error de sintaxis yaml al usar **when** sobre una de las tareas *snapd*.

# [0.1.2](/Ansible_Roles/aptman/compare/0.1.2...main)
* Existe un problema con snap en contenedores docker, no está soportado por ello el test falla, deshabilitamos este tests para este caso concreto.

# [0.1.1](/Ansible_Roles/aptman/compare/0.1.1...main)
* Se corrige un error en el Jenkinsfile, se estaban consigando unos paths a evaluar inexistentes, esto provocaba que el test falle.
* Tambien se prepara un workaround para las pruebas de snap, la imagen de *testing* no tiene instalado snap por ello las pruebas fallan, agregamos unas pre-task para instalarla hasta que se dote de esta feature en la imagen de docker

# [0.1.0](/Ansible_Roles/aptman/compare/0.1.0...main)
* Versión de salida, soporte para apt [instalar, desinstalar], snap [instalar, desinstalar], dpkg [instalar], repositorios y claves [instalar]
