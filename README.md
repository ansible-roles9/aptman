[![pipeline status](https://gitlab.com/ansible-roles9/aptman/badges/development/pipeline.svg)](https://gitlab.com/ansible-roles9/aptman/-/commits/development) ![Project version](https://img.shields.io/gitlab/v/tag/ansible-roles9/aptman)![Project license](https://img.shields.io/gitlab/license/ansible-roles9/aptman)

Role APTMAN
===================

Description
-------------
The purpose of this role is to centralize the management of parcel **deb** type over **apt** package management, **dpkg** or throug **snap | flatpak**, as well as the management of **repositories**  and necessary **keys** to get packages from it.

Requirements
-------------
It has no special requirements

How to use
-------------
    - import_role:
        name: "aptman"
      vars:
        keys:
          - {path: "[url{.asc}]"}
          - {server: "[key_server]", id: "[key_id]"}
        repositories:
          - {name: "repository_name", path: "deb [url] [release] non-free contrib", auth_user: "pepe", auth_pass: "P4pEhOLI"}
        main_packages:
          - {name: "package_name[or_pattern]", present: true[or_false], hold: true[or_false]}
        deb_packages:
          - [url]
        snap_packages:
          - {name: "package_name[or_pattern]", present: true[or_false], classic: true[or_false]}
        flatpak_packages:
          - {name: "package_name[or_pattern]", present: true[or_false]}

ROOT Vars
-------------

* Variable name: `keys`
* Default value: empty [].
* Accepted values: Dictionaries list [key => value].
* Description: Variable intended to contain the list of keys for the purposes of each repository.

----------

* Variable name: `repositories`
* Default value: empty [].
* Accepted values: Dictionaries list [key => value].
* Description: Variable intended to contain the list of repositories to install on the computer.

----------

* Variable name: `main_packages`
* Default value: empty [].
* Accepted values: Dictionaries list [key => value].
* Description: Variable destined to contain the list of packages and their respective states in the device, this is managed by the **apt** package manager.

----------

* Variable name: `deb_packages`
* Default value: empty [].
* Accepted values: List.
* Description: Variable intended to contain the list of packages, this is managed by the **dpkg** package manager.

----------

* Variable name: `snap_packages`
* Default value: empty [].
* Accepted values: Dictionaries list [key => value].
* Description: Variable destined to contain the list of packages and their respective states in the device, this is managed by the **snap** package manager.

----------

* Variable name: `flatpak_packages`
* Default value: empty [].
* Accepted values: Dictionaries list [key => value].
* Description: Variable destined to contain the list of packages and their respective states in the device, this is managed by the **flatpak** package manager.

CHILD Vars for: `keys`
-------------

* Variable name: `path`
* Default value: `NULL`.
* Accepted values: URL
* Description: The url of the key, for example: http://website.com/key.acs.

----------

* Variable name: `server`
* Default value: `NULL`.
* Accepted values: STRING
* Description: Operates in conjunction only with the ***id*** variable, it expects the **host** of the server that contains the key.

----------

* Variable name: `id`
* Default value: `NULL`.
* Accepted values: STRING
* Description: Operates in conjunction only with the ***server*** variable, expects the **id** of the key.

CHILD Vars for: `repositories`
-------------

* Variable name: `name`
* Default value: `NULL`.
* Accepted values: STRING
* Description: The name that will be assigned to the repository on the computer.

----------

* Variable name: `path`
* Default value: `NULL`.
* Accepted values: STRING
* Description: The path and configuration of the repository.

----------

* Variable name: `auth_user`
* Default value: `NULL`.
* Accepted values: STRING
* Description: The username that will be used for authenticate with the server.

----------

* Variable name: `auth_pass`
* Default value: `NULL`.
* Accepted values: STRING
* Description: The password that will be used for authenticate with the server.

CHILD Vars for: `main_packages`
-------------

* Variable name: `name`
* Default value: `NULL`.
* Accepted values: STRING
* Description: The name of the package to install, it is possible to use wildcards, for example: **httpd-***.

----------

* Variable name: `present`
* Default value: `true`.
* Accepted values: BOOLEAN
* Description: The expected state of the packet, supported values ​​are *true* y *false*.

----------

* Variable name: `hold`
* Default value: `false`.
* Accepted values: BOOLEAN
* Description: The expected `hold state` of the packet, supported values ​​are *true* y *false*.

CHILD Vars for: `snap_packages`
-------------

* Variable name: `name`
* Default value: `NULL`.
* Accepted values: STRING
* Description: The name of the package to install, **NO** it is possible to use wildcards*.

----------

* Variable name: `present`
* Default value: `true`.
* Accepted values: BOOLEAN
* Description: The expected state of the packet, supported values ​​are *true* y *false*.

----------

* Variable name: `classic`
* Default value: `false`.
* Accepted values: BOOLEAN
* Description: Specifies if it is a **classic** package, supported values ​​are *true* and *false*.

CHILD Vars for: `flatpak_packages`
-------------

* Variable name: `name`
* Default value: `NULL`.
* Accepted values: STRING
* Description: The ID of the package to install/uninstall, is **NOT** possible use wildcards*.

----------

* Variable name: `present`
* Default value: `true`.
* Accepted values: BOOLEAN
* Description: The expected state of the packet, supported values ​​are *true* y *false*.

Legend
-------------
* NKS => No key sensitive
* KS => Key sensitive
